import 'package:alfa/screen/Faq.dart';
import 'package:alfa/screen/FitnessGoalScreen.dart';
import 'package:alfa/screen/ForgotPassword.dart';
import 'package:alfa/screen/IntroScreen.dart';
import 'package:alfa/screen/LoginScreen.dart';
import 'package:alfa/screen/NotificationScreen.dart';
import 'package:alfa/screen/ProfileScreen.dart';
import 'package:alfa/screen/QuoteScreen.dart';
import 'package:alfa/screen/SettingScreen.dart';
import 'package:alfa/screen/SignupScreen.dart';
import 'package:alfa/screen/Subscribe.dart';
import 'package:alfa/screen/TermsCondition.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:logging/logging.dart';
import 'screen/SplashScreen.dart';
import 'utils/Log.dart';
import 'screen/EmailSent.dart';
import 'screen/DailyLife.dart';
import 'screen/Calculator.dart';

import 'package:alfa/screen/TabbarScreens/TabbarScreen.dart';

import 'screen/WorkoutDetails.dart';
import 'screen/YourQuote.dart';
import 'screen/DailyGoalCheckIn.dart';
import 'screen/WeeklyProgressTracker.dart';
import 'screen/FitnessGoalScreenEdit.dart';
import 'screen/DailyLifeEdit.dart';
import 'screen/ProfileScreenEdit.dart';
import 'screen/PrivacyPolicy.dart';
import 'screen/FeedBack.dart';
import 'screen/MyCards.dart';
import 'screen/AddNewCard.dart';
import 'screen/CardDetails.dart';
import 'screen/AddFood.dart';
import 'screen/FoodHistory.dart';
import 'screen/TrackTab_Details.dart';

import 'dart:async';
import 'package:health/health.dart';
import 'screen/SetWorkouts.dart';


void main() {
  _initLog();
  WidgetsFlutterBinding.ensureInitialized();
  SystemChrome.setSystemUIOverlayStyle(
      SystemUiOverlayStyle(statusBarColor: Colors.transparent));
  SystemChrome.setPreferredOrientations(
      [DeviceOrientation.portraitUp, DeviceOrientation.portraitDown]).then((_) {
        
//    runApp(MyApp());
//    runApp(MyApps());
    runApp(App());
  });
}

void _initLog() {
  Log.init();
  Log.setLevel(Level.ALL);
}

class App extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        debugShowCheckedModeBanner: false,
        title: "Fitness Tracker",
        initialRoute: '/',
        onGenerateRoute: (RouteSettings settings) {
          return MaterialPageRoute(
            builder: (BuildContext context) => makeRoute(
                context: context,
                routeName: settings.name,
                arguments: settings.arguments),
            maintainState: true,
            fullscreenDialog: false,
          );
        });
  }

  Widget makeRoute(
      {@required BuildContext context,
      @required String routeName,
      Object arguments}) {
    final Widget child = _buildRoute(
        context: context, routeName: routeName, arguments: arguments);
    return child;
  }

  Widget _buildRoute({
    @required BuildContext context,
    @required String routeName,
    Object arguments,
  }) {
    switch (routeName) {
      case '/':
        return SplashScreen();
      case '/intro':
        return IntroScreen();
      case '/login':
        return LoginScreen();
      case '/signUp':
        return SignupScreen();
      case '/profile':
        return ProfileScreen();
      case '/terms':
        return TermsCondition();
      case '/fitnessgoal':
        return FitnessGoalScreen();
      case '/Subscribe':
        return Subscribe();
//      case '/quote':
//        return QuoteScreen();
      case '/forgotpassword':
        return ForgotPassword();
      case '/notification':
        return NotificationScreen();
      case '/setting':
        return SettingScreen();
      case '/faq':
        return Faq();
      case '/EmailSent':
        return EmailSent();
      case '/DailyLife':
        return DailyLife();
      case '/TabbarScreen':
        return TabbarScreen();
      case '/WorkoutDetails':
        return WorkoutDetails();
      case '/YourQuote':
        return YourQuote();
      case '/DailyGoalCheckIn':
        return DailyGoalCheckIn();
      case '/WeeklyProgressTracker':
        return WeeklyProgressTracker();
      case '/FitnessGoalScreenEdit':
        return FitnessGoalScreenEdit();
      case '/DailyLifeEdit':
        return DailyLifeEdit();
      case '/ProfileScreenEdit':
        return ProfileScreenEdit();
      case '/PrivacyPolicy':
        return PrivacyPolicy();
      case '/FeedBack':
        return FeedBack();
      case '/MyCards':
        return MyCards();
      case '/AddNewCard':
        return AddNewCard();
      case '/CardDetails':
        return CardDetails();
      case '/AddFood':
        return AddFood();
      case '/FoodHistory':
        return FoodHistory();
      case '/TrackTab_Details':
        return TrackTab_Details();
      case '/Calculator':
        return Calculator();
      case '/SetWorkouts':
        return SetWorkouts();

      default:
        throw 'Route $routeName is not defined';
    }
  }
}




class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

enum AppState { DATA_NOT_FETCHED, FETCHING_DATA, DATA_READY, NO_DATA }

class _MyAppState extends State<MyApp> {
  List<HealthDataPoint> _healthDataList = [];
  AppState _state = AppState.DATA_NOT_FETCHED;

  @override
  void initState() {
    super.initState();
  }

  Future<void> fetchData() async {
    setState(() {
      _state = AppState.FETCHING_DATA;
    });

    /// Get everything from midnight until now
    DateTime endDate = DateTime.now();
    DateTime startDate = DateTime(2020, 01, 01);

    HealthFactory health = HealthFactory();

    /// Define the types to get.
    List<HealthDataType> types = [
      HealthDataType.BODY_MASS_INDEX,
      HealthDataType.WEIGHT,
      HealthDataType.ACTIVE_ENERGY_BURNED,
      HealthDataType.WATER,
      HealthDataType.SLEEP_IN_BED,
      HealthDataType.MINDFULNESS,
    ];

    /// You can request types pre-emptively, if you want to
    /// which will make sure access is granted before the data is requested
//    bool granted = await health.requestAuthorization(types);

    /// Fetch new data
    List<HealthDataPoint> healthData =
    await health.getHealthDataFromTypes(startDate, endDate, types);

    /// Save all the new data points
    _healthDataList.addAll(healthData);

    /// Filter out duplicates
    _healthDataList = HealthFactory.removeDuplicates(_healthDataList);

    /// Print the results
    _healthDataList.forEach((x) => print("Data point: $x"));

    /// Update the UI to display the results
    setState(() {
      _state = _healthDataList.isEmpty ? AppState.NO_DATA : AppState.DATA_READY;
    });
  }

  Widget _contentFetchingData() {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        Container(
            padding: EdgeInsets.all(20),
            child: CircularProgressIndicator(
              strokeWidth: 10,
            )),
        Text('Fetching data...')
      ],
    );
  }

  Widget _contentDataReady() {
    return ListView.builder(
        itemCount: _healthDataList.length,
        itemBuilder: (_, index) {
          HealthDataPoint p = _healthDataList[index];
          return ListTile(
            title: Text("${p.typeString}: ${p.value}"),
            trailing: Text('${p.unitString}'),
            subtitle: Text('${p.dateFrom} - ${p.dateTo}'),
          );
        });
  }

  Widget _contentNoData() {
    return Text('No Data to show');
  }

  Widget _contentNotFetched() {
    return Text('Press the download button to fetch data');
  }

  Widget _content() {
    if (_state == AppState.DATA_READY)
      return _contentDataReady();
    else if (_state == AppState.NO_DATA)
      return _contentNoData();
    else if (_state == AppState.FETCHING_DATA) return _contentFetchingData();

    return _contentNotFetched();
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
          appBar: AppBar(
            title: const Text('Plugin example app'),
            actions: <Widget>[
              IconButton(
                icon: Icon(Icons.file_download),
                onPressed: () {
                  fetchData();
                },
              )
            ],
          ),
          body: Center(
            child: _content(),
          )),
    );
  }
}



