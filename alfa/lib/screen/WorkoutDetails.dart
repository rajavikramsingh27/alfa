import 'dart:ui';
import 'dart:async';
import 'package:flutter/material.dart';
import 'package:alfa/utils/Constents.dart';
import 'package:stop_watch_timer/stop_watch_timer.dart';
import 'package:youtube_player_flutter/youtube_player_flutter.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/services.dart';


String kVideoURL = '';


class WorkoutDetails extends StatefulWidget {

  @override
  _WorkoutDetailsState createState() => _WorkoutDetailsState();
}

class _WorkoutDetailsState extends State<WorkoutDetails> {
  int duration = 901;

  bool isPlay = false;
  String videoId;
  YoutubePlayerController _controller;

  @override
  void initState() {
    // TODO: implement initState
      _controller = YoutubePlayerController(
      initialVideoId:YoutubePlayer.convertUrlToId(kVideoURL),
      flags: YoutubePlayerFlags(
        autoPlay: true,
//        mute: true,
      ),
    );

    super.initState();
  }


  @override
  void deactivate() {
    _controller.pause();
    super.deactivate();
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    var sizeScreen = MediaQuery.of(context).size;

    return Scaffold(
      backgroundColor:Colors.white,
      appBar:AppBar(
          title:Text(
            '',
//            'Upper Body Workouts',
            style:TextStyle(
                fontSize:18,
                fontFamily:AppConstant.kPoppins,
                color:AppConstant.color_blue_dark,
                fontWeight:FontWeight.bold
            ),
          ),
          textTheme:TextTheme(
              title: TextStyle(
                  color: Colors.black
              )
          ),
          centerTitle:false,
          backgroundColor:Colors.white,
          brightness:Brightness.light,
          elevation:0.5,
          leading:IconButton(
              icon:Icon(
                Icons.arrow_back,
                color:Colors.black,
                size:30,
              ),
              onPressed:() {
                Navigator.pop(context);
              })
      ),
      body:Center(
        child:YoutubePlayer(
          controller: _controller,
          liveUIColor: Colors.amber,
        ),
      )
    );
  }
}

