import 'package:alfa/screen/WorkoutDetails.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:alfa/utils/Constents.dart';
import 'package:alfa/res.dart';
import 'package:g2x_week_calendar/g2x_simple_week_calendar.dart';
import 'package:flutter_swiper/flutter_swiper.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:toast/toast.dart';
import 'package:intl/intl.dart';


import 'package:flutter/rendering.dart';
import 'package:image_picker/image_picker.dart';
import '../res.dart';
import 'dart:io';
import 'package:firebase_storage/firebase_storage.dart';


class SetWorkouts extends StatefulWidget {
  @override
  _SetWorkoutsState createState() => _SetWorkoutsState();
}

class _SetWorkoutsState extends State<SetWorkouts> {
  String strName = '';
  String strExerciseName = '';
  String strTitleBody = '';
  String strParamTitle = '';
  String strUpper_body = '';
  String strCore_cardio = '';
  String strFull_body = '';
  String strLower_body = '';

  String downloadUrl = '';
  String formattedDate = '';

  String strThumbnail = 'Thumbnail';
  File _image;
  final picker = ImagePicker();

  DateTime setDate;

  Map<String, dynamic> dictLive = {kLink:'',kThumbnail:'',kLiveOn:''};

  TextEditingController txtLink = TextEditingController();
  TextEditingController txtLiveOn = TextEditingController();

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
  }

  List<Map<String, dynamic>> arrQuotes = [];

  @override
  void initState() {
    Future.delayed(Duration(milliseconds:1),() async {
      var now = DateTime.now();
      var formatter = DateFormat('yyyy_MMM_dd');
      formattedDate = formatter.format(now);

//      getExercises(formattedDate);
//
//      QuerySnapshot querySnapshot = await Firestore.instance.collection(tblQuotes).getDocuments();
//      arrQuotes = querySnapshot.documents.map((DocumentSnapshot doc) {
//        return doc.data;
//      }).toList();
//      print(arrQuotes);
//      setState(() {
//
//      });
//
//      FirebaseAuth.instance.currentUser().then((value) {
//        Firestore.instance.collection(tblUserDetails).document(
//            value.email + kFireBaseConnect + value.uid).get().then((value) {
//          var dictUserDetails = value.data;
//          strName = dictUserDetails[kFirstName]+' '+dictUserDetails[kLastName];
//          setState(() {
//
//          });
//        }).catchError((error) {
//          Toast.show(
//              error.toString(),
//              context,
//              backgroundColor: HexColor(redColor)
//          );
//        }).catchError((error) {
////          dismissLoading(context);
//          Toast.show(
//              error.toString(),
//              context,
//              backgroundColor: HexColor(redColor)
//          );
//        });
//      });
      getData();
    });
    // TODO: implement initState
    super.initState();
  }

  getData() {
    showLoading(context);
    Firestore.instance.collection(tblExercises).document(formattedDate).get().then((value) {
      print(value.data);
      dismissLoading(context);

      if (value.data == null) {
        setData();
      }
    }).catchError((error) {
      print(error.toString());
      setData();
      dismissLoading(context);

//      Toast.show(error.toString(), context,
//          backgroundColor:HexColor(redColor)
//      );
    });
  }

  setData() {
    showLoading(context);
    Firestore.instance
        .collection(tblExercises)
        .document(formattedDate)
        .setData({
      kUpper_body:'',
      kCore_cardio:'',
      kFull_body:'',
      kLower_body:'',
      kLive:''
    }).then((value) {
      dismissLoading(context);
    }).catchError((error) {
      print(error.toString());
      dismissLoading(context);
      Toast.show(error.toString(), context,
          backgroundColor: HexColor(redColor));
    });
  }

  updateData(String strExerciseTitle) {
    showLoading(context);
    Firestore.instance.collection(tblExercises).document(formattedDate).updateData({
      strExerciseTitle:downloadUrl
    }).then((value) {
      downloadUrl = '';
      dismissLoading(context);
      Toast.show(
          '${strTitleBody} is updated', context,
          backgroundColor: HexColor(greenColor)
      );
    }).catchError((error) {
      dismissLoading(context);
      Toast.show(error.toString(), context,
          backgroundColor: HexColor(redColor));
    });

  }

  updateLiveData(String strExerciseTitle,Map LiveData) {
    showLoading(context);
    Firestore.instance.collection(tblExercises).document(formattedDate).updateData({
      strExerciseTitle:LiveData
    }).then((value) {
      downloadUrl = '';
      dismissLoading(context);

      Toast.show(
          '${strTitleBody} is updated', context,
          backgroundColor: HexColor(greenColor));
    }).catchError((error) {
      dismissLoading(context);
      Toast.show(error.toString(), context,
          backgroundColor: HexColor(redColor)
      );
    });

  }

//  getExercises(String date) async {
//    dictLive = {kLink:'',kThumbnail:'',kLiveOn:''};
//
//    strUpper_body = '';
//    strCore_cardio = '';
//    strFull_body = '';
//    strLower_body = '';
//
//    Firestore.instance.collection(tblExercises).document(date).get().then((value) {
//      print(value.data);
//
//      strUpper_body = value.data[kUpper_body];
//      strCore_cardio = value.data[kCore_cardio];
//      strFull_body = value.data[kFull_body];
//      strLower_body = value.data[kLower_body];
//      dictLive = value.data[kLive];
//
//      setState(() {
//
//      });
//    }).catchError((error) {
////      dismissLoading(context);
//      Toast.show(
//          error.toString(),
//          context,
//          duration:2,
//          gravity:Toast.BOTTOM,
//          backgroundColor:HexColor(redColor)
//      );
//      setState(() {
//
//      });
//    });
//  }

  Future<void> _uploadVideo(String tblName) async {
    showLoading(context);

    var ref = FirebaseStorage.instance.ref().child(tblName).child(formattedDate+"_"+strParamTitle);
    var uploadTask = ref.putFile(_image, StorageMetadata(contentType: 'video/mp4'));
//    ref.putFile(_image);
    var storageTaskSnapshot = await uploadTask.onComplete;
    downloadUrl = await storageTaskSnapshot.ref.getDownloadURL();
    dismissLoading(context);

//    StorageUploadTask uploadTask = ref.putFile(_image, StorageMetadata(contentType: 'video/mp4'));
//
//    Uri downloadUrl = (await uploadTask.future).downloadUrl;
//
//    final String url = downloadUrl.toString();


//    if (strParamTitle.contains('live')) {
//      print('updating live...');
//      strThumbnail = 'Thumbnail is selected.';
//
//      setState(() {
//
//      });
//    } else {
      updateData(strParamTitle);
//    }

  }

  Future<void> _uploadFile(String tblName) async {
      showLoading(context);

      var ref = FirebaseStorage.instance.ref().child(tblName).child(formattedDate+"_"+strParamTitle);
      var uploadTask = ref.putFile(_image);
      var storageTaskSnapshot = await uploadTask.onComplete;
      downloadUrl = await storageTaskSnapshot.ref.getDownloadURL();
      dismissLoading(context);

      if (strParamTitle.contains('live')) {
        print('updating live...');
        strThumbnail = 'Thumbnail is selected.';

        setState(() {

        });
      } else {
        updateData(strParamTitle);
      }
  }

  Future openCamera() async {
    final pickedFile = await picker.getVideo(source: ImageSource.camera);
    setState(() {
      _image = File(pickedFile.path);
      _uploadVideo('exercises/');
    });
  }

  Future openGallery() async {
    final pickedFile = await picker.getVideo(source: ImageSource.gallery);
    setState(() {
      _image = File(pickedFile.path);
      _uploadVideo('exercises/');
    });
  }

  Future openCameraForImage() async {
    final pickedFile = await picker.getImage(source: ImageSource.camera);
    setState(() {
      _image = File(pickedFile.path);
      _uploadFile('liveThumbnail/');
    });
  }

  Future openGalleryForImage() async {
    final pickedFile = await picker.getImage(source: ImageSource.gallery);
    setState(() {
      _image = File(pickedFile.path);
      _uploadFile('liveThumbnail/');
    });
  }

  _settingModalBottomSheet(context,) {
    showModalBottomSheet(
        backgroundColor:Colors.transparent,
        context: context,
        builder:(BuildContext bc) {
          return Container(
              height:300,
              decoration:BoxDecoration(
                color:Colors.white,
                borderRadius:BorderRadius.only(
                    topRight:Radius.circular(20),
                    topLeft:Radius.circular(20)
                ),
              ),
              child:Center(
                child:Column(
                  children:<Widget>[
                    SizedBox(height:30),
                    Text(
                        '$strTitleBody \nPlease select an option.',
                        textAlign:TextAlign.center,
                        style:TextStyle(
                          fontSize:18,
                          fontFamily:AppConstant.kPoppins,
                          color:AppConstant.color_blue_dark,
                          fontWeight:FontWeight.bold
                      ),
                    ),
                    SizedBox(height:10),
                    FlatButton(
                      child:Text(
                        'Camera',
                        style:TextStyle(
                            fontSize:16,
                            fontFamily:AppConstant.kPoppins,
                            color:AppConstant.color_blue_dark,
                            fontWeight:FontWeight.normal
                        ),
                      ),
                      onPressed:() {
                        Navigator.pop(context);
                        if (strParamTitle.contains('live')) {
                          openCameraForImage();
                        } else {
                          openCamera();
                        }
                      },
                    ),
                    FlatButton(
                      child:Text(
                        'Gallery',
                        style:TextStyle(
                            fontSize:16,
                            fontFamily:AppConstant.kPoppins,
                            color:AppConstant.color_blue_dark,
                            fontWeight:FontWeight.normal
                        ),
                      ),
                      onPressed:() {
                        Navigator.pop(context);
                        if (strParamTitle.contains('live')) {
                          openGalleryForImage();
                        } else {
                          openGallery();
                        }
                      },
                    ),
                    SizedBox(height:30,),
                    FlatButton(
                      child:Text(
                        'Cancel',
                        style:TextStyle(
                            fontSize:18,
                            fontFamily:AppConstant.kPoppins,
                            color:Colors.red,
                            fontWeight:FontWeight.normal
                        ),
                      ),
                      onPressed:() {
                        Navigator.pop(context);
                      },
                    ),
                  ],
                ),
              )
          );
        }
    );
  }

  showUploadLiveWorkout(context,) {
    txtLink.text = '';
    txtLiveOn.text = '';

    showModalBottomSheet(
        backgroundColor:Colors.transparent,
        context: context,
        builder:(BuildContext bc) {
          return Container(
              height:380,
              decoration:BoxDecoration(
                color:Colors.white,
                borderRadius:BorderRadius.only(
                    topRight:Radius.circular(20),
                    topLeft:Radius.circular(20)
                ),
//              border: Border.all(width:3,color: Colors.green,style: BorderStyle.solid)
              ),
              child:Container(
                padding:EdgeInsets.only(left:25,right:25),
                child:SingleChildScrollView(
                  child:Column(
                    children:[
                      SizedBox(height:30),
                      Text(
                        strTitleBody,
                        textAlign:TextAlign.center,
                        style:TextStyle(
                            fontSize:18,
                            fontFamily:AppConstant.kPoppins,
                            color:AppConstant.color_blue_dark,
                            fontWeight:FontWeight.bold
                        ),
                      ),
                      SizedBox(height:10),
                      TextField(
                      controller:txtLink,
                        cursorColor:Color(0xff84828F),
                        decoration:InputDecoration(
                          //labelText: title ,  // you can change this with the top text  like you want
                            labelText:'Link',
                            hintStyle:TextStyle(
                                fontFamily:AppConstant.kPoppins,
                                fontWeight:FontWeight.normal,
                                fontSize:15
                            ),
                            /*border: InputBorder.none,*/
                            fillColor: Color(0xff84828F)
                        ),
                      ),
                      SizedBox(height:10),
                      TextField(
                      controller:txtLiveOn,
                        cursorColor:Color(0xff84828F),
                        decoration:InputDecoration(
                          //labelText: title ,  // you can change this with the top text  like you want
                            labelText:'Live On (Zoom, Youtube, Facebook, etc...)',
                            hintStyle:TextStyle(
                                fontFamily:AppConstant.kPoppins,
                                fontWeight:FontWeight.normal,
                                fontSize:15
                            ),
                            /*border: InputBorder.none,*/
                            fillColor:Color(0xff84828F)
                        ),
                      ),
                      SizedBox(height:10),
                      FlatButton(
                        padding:EdgeInsets.all(0),
                        child:Container(
                          padding:EdgeInsets.only(left:0,right:0,top:10,bottom:14),
                          width:double.infinity,
                          alignment:Alignment.centerLeft,
                          decoration:BoxDecoration(
                            border:Border(
                              bottom:BorderSide(width:1.0, color:Color(0xff84828F)),
                            ),
                            color: Colors.white,
                          ),
                          child:Text(
                            strThumbnail,
                            textAlign:TextAlign.left,
                            style:TextStyle(
                                color:Color(0xff84828F),
                                fontFamily:AppConstant.kPoppins,
                                fontWeight:FontWeight.normal,
                                fontSize:15
                            ),
                          ),
                        ),
                        onPressed:() {
                          _settingModalBottomSheet(context);
                        },
                      ),
                      SizedBox(height:30),
                      Container(
                        height:50,
                        width:MediaQuery.of(context).size.width,
                        decoration:kButtonThemeGradientColor(),
                        child:FlatButton(
                          padding:EdgeInsets.all(0),
                          child:Text(
                            'Upload',
                            style:TextStyle(
                                color:Colors.white,
                                fontFamily:AppConstant.kPoppins,
                                fontWeight:FontWeight.normal,
                                fontSize:18
                            ),
                          ),
                          onPressed:() {
                            if (txtLink.text.isEmpty) {
                              Toast.show('Enter a link for live', context,
                                backgroundColor:HexColor(redColor)
                              );
                            } else {
                              Map dictLiveData = {
                                kLink:txtLink.text,
                                kLiveOn:txtLiveOn.text,
                                kThumbnail:downloadUrl
                              };
                              dismissLoading(context);
                              updateLiveData(strParamTitle,dictLiveData);
                            }
                          },
                        ),
                      ),
                      SizedBox(height:20),
                    ],
                  ),
                )
              )
          );
        }
    );
  }

  @override
  Widget build(BuildContext context) {
    var sizeScreen = MediaQuery.of(context).size;

    return Scaffold(
      backgroundColor: Colors.white,
      body:Stack(
        children: [
//          Container(
//            height: 250,
//            decoration: BoxDecoration(
//                borderRadius: BorderRadius.only(
//                    bottomRight: Radius.circular(30),
//                    bottomLeft: Radius.circular(30)),
//                gradient: LinearGradient(
//                  colors: [HexColor('2E4877'), HexColor('29364E')],
//                  begin: FractionalOffset.topCenter,
//                  end: FractionalOffset.bottomCenter,
//                )),
//          ),
          SafeArea(
            child:Container(
              color:Colors.transparent,
              child:SingleChildScrollView(
                  padding: EdgeInsets.only(top:60 - MediaQuery.of(context).padding.top),
                  physics: BouncingScrollPhysics(),

                  child: Column(
                      children:<Widget>[
                        Container(
                          width:MediaQuery.of(context).size.width-44,
                          margin:EdgeInsets.only(left:0, right: 22),
                          child:Row(
                            crossAxisAlignment:CrossAxisAlignment.start,
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: <Widget>[
                              IconButton(
                                icon:Icon(
                                  Icons.arrow_back,
                                  color:AppConstant.color_blue_dark,
                                  size:30,
                                ),
                                iconSize:30,
                                onPressed: () {
                                  Navigator.pop(context);
                                },
                              ),
                              Container(
//                                color:Colors.red,
                                width:MediaQuery.of(context).size.width-100,
                                child:Text(
                                  'Hello, Admin!',
                                  style: TextStyle(
                                      fontSize: 24,
                                      fontFamily: AppConstant.kPoppins,
                                      fontWeight: FontWeight.normal,
                                      color:AppConstant.color_blue_dark
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                        Container(
//                                color:Colors.red,
                          width:MediaQuery.of(context).size.width-130,
                          child:Text(
                            'You can set exercises and live video for a particular date.',
                            style: TextStyle(
                                fontSize:14,
                                fontFamily: AppConstant.kPoppins,
                                fontWeight: FontWeight.normal,
                                color:AppConstant.color_blue_dark),
                          ),
                        ),
                        Container(
                          decoration: BoxDecoration(
                            border:Border(
                              bottom:BorderSide(width:1.0, color:Color(0xff84828F)),
                            ),
                            color: Colors.white,
                          ),
//                          margin:EdgeInsets.only(top:10),
                          padding:EdgeInsets.only(bottom:20),
                          child:G2xSimpleWeekCalendar(
                            90.0, DateTime.now(),
                            dateCallback:(date) {
                              var formatter = DateFormat('yyyy_MMM_dd');
                              formattedDate = formatter.format(date);

                              getData();
//                              getExercises(formattedDate);
                            },

                            selectedDateBackgroundColor:Colors.white,
                            selectedDateTextColor:AppConstant.color_blue_dark,

                            typeCollapse: false,
                            selectedTextStyle:TextStyle(
                                color:Colors.white,
                                fontFamily:AppConstant.kPoppins,
                                fontWeight:FontWeight.normal,
                                fontSize:16),
                            selectedBackgroundDecoration: BoxDecoration(
                                color:AppConstant.color_blue_dark,
                                borderRadius: BorderRadius.circular(30)
                            ),
                            strWeekDays: ['Su','Mo', 'Tu', 'We', 'Th', 'Fr', 'Sa',],
                            defaultTextStyle: TextStyle(
                                color: AppConstant.color_blue_dark,
                                fontFamily: AppConstant.kPoppins,
                                fontWeight: FontWeight.normal,
                                fontSize: 16),
                          ),
                        ),
                        Container(
                          margin:EdgeInsets.only(top:20, left: 20, right: 20),
                          height:110,
                          child:Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: <Widget>[
                              Container(
                                  width: (sizeScreen.width - 70) / 2,
                                  padding: EdgeInsets.only(
                                      top: 10, left: 16, right: 10),
                                  alignment: Alignment.centerLeft,
                                  decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(20),
                                      gradient: LinearGradient(
                                        colors: [
                                          HexColor('#84828F'),
                                          HexColor('424148')
                                        ],
                                        begin: FractionalOffset.topCenter,
                                        end: FractionalOffset.bottomCenter,
                                      )),
                                  child: FlatButton(
                                    padding: EdgeInsets.all(0),
                                    child: Container(
                                      width: double.infinity,
                                      height: double.infinity,
                                      child: Stack(
                                        children: <Widget>[
                                          Text(
                                            'Lower Body\nWorkouts',
                                            style:TextStyle(
                                                color:Colors.white,
                                                fontFamily:
                                                AppConstant.kPoppins,
                                                fontWeight: FontWeight.normal,
                                                fontSize:15),
                                          ),
                                          Positioned(
                                            right: 0,
                                            bottom: 0,
                                            child:Image.asset(
                                              Res.lowerBody,
                                              height:88,
                                              width:48,
                                            ),
                                          )
                                        ],
                                      ),
                                    ),
                                    onPressed: () {
                                      strTitleBody = 'Lower body workouts';
                                      strParamTitle = kLower_body;

//                                      _settingModalBottomSheet(context);

                                      showUploadLiveWorkout(context);

//                                      kVideoURL = strLower_body;
//                                      if (kVideoURL.isNotEmpty) {
//                                        Navigator.pushNamed(context, '/WorkoutDetails');
//                                      } else {
//                                        Toast.show(
//                                            'Workout is not available.',
//                                            context,
//                                            backgroundColor: HexColor(redColor)
//                                        );
//                                      }

                                    },
                                  )),
                              FlatButton(
                                padding:EdgeInsets.all(0),
                                textColor:Colors.white,
                                child:Container(
                                    width:(sizeScreen.width - 70) / 2,
                                    padding:EdgeInsets.only(
                                        top:10, left: 16, right: 10),
                                    alignment: Alignment.centerLeft,
                                    decoration: BoxDecoration(
                                        borderRadius: BorderRadius.circular(20),
                                        gradient: LinearGradient(
                                          colors:[
                                            HexColor('3ACBAB'),
                                            HexColor('0EAA88')
                                          ],
                                          begin:FractionalOffset.topCenter,
                                          end:FractionalOffset.bottomCenter,
                                        )),
                                    child:Container(
                                      width:double.infinity,
                                      height:double.infinity,
                                      child:Stack(
                                        children:<Widget>[
                                          Text(
                                            'Upper Body\nWorkouts',
                                            style: TextStyle(
                                                color: Colors.white,
                                                fontFamily: AppConstant.kPoppins,
                                                fontWeight: FontWeight.normal,
                                                fontSize: 15),
                                          ),
                                          Positioned(
                                            right: 0,
                                            bottom: 0,
                                            child: Image.asset(
                                              Res.upper,
                                              height: 63,
                                            ),
                                          )
                                        ],
                                      ),
                                    )),
                                onPressed:() {
                                  strTitleBody = 'Upper body workouts';
                                  strParamTitle = kUpper_body;
//                                  _settingModalBottomSheet(context);

                                  showUploadLiveWorkout(context);

//                                  kVideoURL = strUpper_body;
//                                  if (kVideoURL.isNotEmpty) {
//                                    Navigator.pushNamed(
//                                        context, '/WorkoutDetails');
//                                  } else {
//                                    Toast.show(
//                                        'Workout is not available.',
//                                        context,
//                                        backgroundColor: HexColor(redColor)
//                                    );
//                                  }
                                },
                              )
                            ],
                          ),
                        ),
                        Container(
                          margin:EdgeInsets.only(top: 30, left: 20, right: 20),
                          height:110,
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: <Widget>[
                              FlatButton(
                                padding:EdgeInsets.all(0),
                                textColor:Colors.white,
                                child:Container(
                                    width: (sizeScreen.width - 70) / 2,
                                    padding: EdgeInsets.only(
                                        top: 10, left: 16, right: 10),
                                    alignment: Alignment.centerLeft,
                                    decoration: BoxDecoration(
                                        borderRadius: BorderRadius.circular(20),
                                        gradient: LinearGradient(
                                          colors: [
                                            HexColor('34486F'),
                                            HexColor('314467')
                                          ],
                                          begin: FractionalOffset.topCenter,
                                          end: FractionalOffset.bottomCenter,
                                        )),
                                    child: Container(
                                      width: double.infinity,
                                      height: double.infinity,
                                      child: Stack(
                                        children: <Widget>[
                                          Text(
                                            'Full Body\nWorkouts',
                                            style: TextStyle(
                                                color: Colors.white,
                                                fontFamily: AppConstant.kPoppins,
                                                fontWeight: FontWeight.normal,
                                                fontSize: 15),
                                          ),
                                          Positioned(
                                            right: 0,
                                            bottom: 0,
                                            child: Image.asset(
                                              Res.fullBody,
//                                      fit:BoxFit.cover,
                                              height: 100,
                                              width: 40,
                                            ),
                                          )
                                        ],
                                      ),
                                    )),
                                onPressed:() {
                                  strTitleBody = 'Full body workouts';
                                  strParamTitle = kFull_body;
//                                  _settingModalBottomSheet(context);
                                  showUploadLiveWorkout(context);

//                                  kVideoURL = strFull_body;
//                                  if (kVideoURL.isNotEmpty) {
//                                    Navigator.pushNamed(
//                                        context, '/WorkoutDetails');
//                                  } else {
//                                    Toast.show(
//                                        'Workout is not available.',
//                                        context,
//                                        backgroundColor: HexColor(redColor)
//                                    );
//                                  }
                                },
                              ),
                              FlatButton(
                                padding:EdgeInsets.all(0),
                                textColor:Colors.white,
                                child:Container(
                                    width: (sizeScreen.width - 70) / 2,
                                    padding: EdgeInsets.only(top: 10, left: 16, right:0),
                                    alignment: Alignment.centerLeft,
                                    decoration: BoxDecoration(
                                        borderRadius: BorderRadius.circular(20),
                                        gradient: LinearGradient(
                                          colors: [
                                            HexColor('F4C3AC'),
                                            HexColor('F3976B')
                                          ],
                                          begin:FractionalOffset.topCenter,
                                          end:FractionalOffset.bottomCenter,
                                        )),
                                    child: Container(
                                      width: double.infinity,
                                      height: double.infinity,
                                      child: Stack(
                                        children: <Widget>[
                                          Text(
                                            'Core & Cardio\nWorkouts',
                                            style: TextStyle(
                                                color: Colors.white,
                                                fontFamily: AppConstant.kPoppins,
                                                fontWeight: FontWeight.normal,
                                                fontSize: 15),
                                          ),
                                          Positioned(
                                            right: 0,
                                            bottom: 0,
                                            child: Image.asset(
                                              Res.core,
                                              height: 90,
                                              width: 130,
                                            ),
                                          )
                                        ],
                                      ),
                                    )),
                                onPressed:() {
                                  strTitleBody = 'Core & Cardio workouts';
                                  strParamTitle = kCore_cardio;
//                                  _settingModalBottomSheet(context);

                                  showUploadLiveWorkout(context);

//                                  kVideoURL = strCore_cardio;
//                                  if (kVideoURL.isNotEmpty) {
//                                    Navigator.pushNamed(
//                                        context, '/WorkoutDetails');
//                                  } else {
//                                    Toast.show(
//                                        'Workout is not available.',
//                                        context,
//                                        backgroundColor: HexColor(redColor)
//                                    );
//                                  }
                                },
                              )
                            ],
                          ),
                        ),
                        Visibility(
                          visible:true,
//                          (dictLive[kLink].isEmpty) ? false : true,
                          child:Column(
                            children: [
                              Container(
                                  width:double.infinity,
                                  height:30,
                                  margin:EdgeInsets.only(left: 20, right: 20, top: 30),
                                  child:RichText(
                                    text: TextSpan(
                                      text:'Upload workout of the day.',
//                                    style: DefaultTextStyle.of(context).style,
                                      style: TextStyle(
                                          color: Colors.black,
                                          fontFamily: AppConstant.kPoppins,
                                          fontWeight: FontWeight.bold,
                                          fontSize: 18),
                                      children: <TextSpan>[
                                        TextSpan(
                                          text:''+dictLive[kLiveOn],
                                          style:TextStyle(
                                              color:Colors.blue,
                                              fontFamily:AppConstant.kPoppins,
                                              fontWeight:FontWeight.normal,
                                              fontSize:18),
                                        ),
                                      ],
                                    ),
                                  )
                              ),
                              FlatButton(
                                padding:EdgeInsets.all(0),
                                child:Container(
                                  margin:EdgeInsets.only(left: 20, right: 20, top: 30),
                                  width:double.infinity,
                                  height:210,
                                  child:Stack(
                                    children: <Widget>[
                                      Container(
                                        width:double.infinity,
                                        height:210,
                                        child:ClipRRect(
                                            borderRadius:BorderRadius.circular(10),
                                            child:FadeInImage(
                                              fit:BoxFit.fill,
                                              height:300,
                                              width:double.infinity,
                                              image:NetworkImage(dictLive[kThumbnail]),
                                              placeholder:AssetImage(Res.ImageThumbnailVideo),
                                            )
                                        ),
                                      ),
//                                    Container(
//                                      width: double.infinity,
//                                      height: 210,
//                                      decoration: BoxDecoration(
//                                        color: HexColor('29364E').withOpacity(0.4),
//                                        borderRadius: BorderRadius.circular(10),
//                                      ),
//                                    ),
//                                    Container(
//                                        child: Center(
//                                          child: IconButton(
//                                              icon: Image.asset(
//                                                Res.play,
//                                              ),
//                                              onPressed: () async {
//                                                var url = dictLive[kLink].toString();
//                                                if (await canLaunch(url)) {
//                                                  await launch(url);
//                                                } else {
//                                                  throw 'Could not launch $url';
//                                                }
//                                              }
//                                          ),
//                                        )),
                                    ],
                                  ),
                                ),
                                onPressed:() {
                                  strTitleBody = 'Upload workout of the day';
                                  strParamTitle = kLive;
                                  showUploadLiveWorkout(context);
                                },
                              )
                            ],
                          ),
                        ),
                        SizedBox(
                          height: 40,
                        )
                      ]
                  )
              )
            ),
          )
        ],
      )
    );
  }
}
