//
//import 'package:alfa/utils/Constents.dart';
//import 'package:flutter/material.dart';
//import 'package:flutter_statusbarcolor/flutter_statusbarcolor.dart';
//
//import '../res.dart';
//
//class QuoteScreen extends StatefulWidget {
//  @override
//  State<StatefulWidget> createState() => QuoteScreenState ();
//}
//class QuoteScreenState extends State<QuoteScreen> {
//
//
//  @override
//  Widget build(BuildContext context) {
//    FlutterStatusbarcolor.setStatusBarColor(Colors.white);
//    FlutterStatusbarcolor.setStatusBarWhiteForeground(false);
//    return Scaffold(
//      backgroundColor: Colors.white,
//      body: SafeArea(
//          child: SingleChildScrollView(
//              child: Container(
//                color: Colors.white,
//                child: Column(
//                  mainAxisSize: MainAxisSize.max,
//                  crossAxisAlignment: CrossAxisAlignment.center,
//                  children: <Widget>[
//                    Row(
//                      crossAxisAlignment: CrossAxisAlignment.start,
//                      children: <Widget>[
//                        Container(
//                          margin: EdgeInsets.only(left: 16,top: 16),
//                          child: GestureDetector(
//                            onTap: (){
//
//                              Navigator.pop(context,false);
//                            },
//                            child: Image.asset(Res.ic_back,width: 20,height: 20,),
//                          )
//                        ),
//                        Padding(
//                          padding: EdgeInsets.only(left: 16,top: 16),
//                          child: Text("Your Quote",style: TextStyle(fontSize: 18,fontFamily: AppConstant.fontBold,color: Colors.black),),
//                        ),
//
//
//                      ],
//                    ),
//
//
//                    SizedBox(
//                      height: 30,
//                    ),
//
//                    Row(
//                      crossAxisAlignment: CrossAxisAlignment.start,
//                      children: <Widget>[
//                        Padding(
//                          padding: EdgeInsets.only(left: 16),
//                          child:  Text("Name",style: TextStyle(fontSize: 15,fontFamily: AppConstant.fontBold,color: AppConstant.color_blue_dark),),
//                        )
//                      ],
//                    ),
//                    SizedBox(
//                      height: 8,
//                    ),
//                    Container(
//                      decoration: BoxDecoration(
//                          borderRadius: BorderRadius.circular(6),
//                          border: Border.all(color: Colors.grey.shade300)
//                      ),
//                      margin: EdgeInsets.only(left: 16,right: 16),
//                      child:  Padding(
//                          padding: EdgeInsets.only(left: 5),
//                          child:Column(
//                            crossAxisAlignment: CrossAxisAlignment.start,
//                            children: <Widget>[
//                              TextField(
//                                  cursorColor: Color(0xff84828F),
//                                  decoration: InputDecoration(
//                                    //labelText: title ,  // you can change this with the top text  like you want
//                                      hintText: "Enter Name",
//                                      hintStyle: TextStyle(color: Colors.grey.shade300),
//                                      border: InputBorder.none,
//                                      fillColor: Color(0xff84828F))),
//                            ],
//                          )
//                      ),
//                    ),
//
//                    SizedBox(
//                      height: 40,
//                    ),
//                    Row(
//                      crossAxisAlignment: CrossAxisAlignment.start,
//                      children: <Widget>[
//                        Padding(
//                          padding: EdgeInsets.only(left: 16),
//                          child:  Text("Your Quotes",style: TextStyle(fontSize: 15,fontFamily: AppConstant.fontBold,color: AppConstant.color_blue_dark),),
//                        )
//                      ],
//                    ),
//                    SizedBox(
//                      height: 8,
//                    ),
//                    Container(
//                      height: 150,
//                      decoration: BoxDecoration(
//                          borderRadius: BorderRadius.circular(6),
//                          border: Border.all(color: Colors.grey.shade300)
//                      ),
//                      margin: EdgeInsets.only(left: 16,right: 16),
//                      child:  Padding(
//                          padding: EdgeInsets.only(left: 5),
//                          child:Column(
//                            crossAxisAlignment: CrossAxisAlignment.start,
//                            children: <Widget>[
//                              TextField(
//                                  cursorColor: Color(0xff84828F),
//                                  decoration: InputDecoration(
//                                    //labelText: title ,  // you can change this with the top text  like you want
//                                      hintText: " Enter tour quotes",
//                                      hintStyle: TextStyle(color: Colors.grey.shade300),
//                                      border: InputBorder.none,
//                                      fillColor: Color(0xff84828F))),
//                            ],
//                          )
//                      ),
//                    ),
//                    SizedBox(
//                      height: 80,
//                    ),
//                    Container(
//                      margin: EdgeInsets.only(top: 16,bottom: 16),
//                      width: double.infinity,
//                      alignment: Alignment.center,
//                      child: Padding(
//                          padding: EdgeInsets.only(),
//                          child: ButtonTheme(
//                            minWidth: 300,
//                            height: 50,
//                            child: RaisedButton(
//
//                              textColor: Colors.white,
//                              color: AppConstant.color_blue_dark,
//                              child: Text("Send", style: TextStyle(fontSize: 16, fontFamily: AppConstant.fontBold,color: Colors.white),
//                              ),
//                              onPressed: () {
//                                  Navigator.pushReplacementNamed(context, '/addcard');
//                              },
//                              shape: new RoundedRectangleBorder(
//                                borderRadius: new BorderRadius.circular(10.0),
//                              ),
//                            ),
//                          )
//                      ),
//                    ),
//                  ],
//                ),
//              )
//          )
//      ),
//    );
//  }
//}